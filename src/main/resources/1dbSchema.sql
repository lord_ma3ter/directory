DROP TABLE IF EXISTS repositories;
DROP TABLE IF EXISTS contacts;

CREATE TABLE contacts
(
    `id`           INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name`         VARCHAR(200) NOT NULL,
    `phone_number` VARCHAR(200) NOT NULL,
    `email`        VARCHAR(200) DEFAULT NULL,
    `organization` VARCHAR(200) DEFAULT NULL,
    `github`       VARCHAR(200) DEFAULT NULL
);

CREATE TABLE repositories
(
    `id`         INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name`       VARCHAR(250),
    `contact_id` INT NOT NULL,
    CONSTRAINT contacts_repositories_fk FOREIGN KEY (contact_id) references contacts (id)
);
