package com.hakim.directory.service.mapper;

import com.hakim.directory.domain.Contact;
import com.hakim.directory.service.dto.ContactDto;
import com.hakim.directory.service.dto.ContactFullDto;
import com.hakim.directory.service.dto.RepositoryDto;

import java.util.ArrayList;
import java.util.List;

public class ContactMapper {

	public static Contact contactDtoToContact(ContactDto c) {
		return new Contact(c.getName(), c.getEmail(),c.getPhoneNumber(),
				c.getOrganization(), c.getGithub());
	}

	public static ContactDto contactToContactDto(Contact c) {
		return new ContactDto(c.getName(), c.getEmail(),
				c.getOrganization(), c.getGithub(),c.getPhoneNumber());
	}

	public static List<ContactDto> contactsToContactDtos(List<Contact> c) {
		List<ContactDto> list=new ArrayList<>();

		for (Contact a: c){
			list.add(new ContactDto(a.getName(),a.getEmail(),a.getOrganization(),a.getGithub(),a.getPhoneNumber()));
		}
		return list;
	}

	public static ContactFullDto contactToContactFullDto(Contact c) {
		List<RepositoryDto> gitRepositories =
				RepositoryMapper.repositoryListToRepositoryDto(c.getGithubRepoList());
		return new ContactFullDto(c.getName(), c.getPhoneNumber(),c.getEmail(),
				c.getOrganization(), c.getGithub(), gitRepositories);
	}
}
