package com.hakim.directory.service.mapper;

import com.hakim.directory.domain.Repository;
import com.hakim.directory.service.dto.RepositoryDto;

import java.util.ArrayList;
import java.util.List;

public class RepositoryMapper {

	public static Repository repositoryDtoToRepository(RepositoryDto g) {
		return new Repository(g.getName());
	}

	public static List<Repository> repositoryDtosToRepositoryList(List<RepositoryDto> g) {
		List<Repository> repositoryList = new ArrayList<>();
		for (RepositoryDto a : g) {
			repositoryList.add(new Repository(a.getName()));
		}
		return repositoryList;
	}

	public static List<RepositoryDto> repositoryListToRepositoryDto(List<Repository> g) {
		List<RepositoryDto> list = new ArrayList<>();
		for (Repository a : g) {
			list.add(new RepositoryDto(a.getName()));
		}
		return list;
	}

}
