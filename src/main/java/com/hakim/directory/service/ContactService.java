package com.hakim.directory.service;

import com.hakim.directory.domain.Contact;
import com.hakim.directory.domain.Repository;
import com.hakim.directory.repository.ContactRepository;
import com.hakim.directory.service.dto.ContactDto;
import com.hakim.directory.service.dto.RepositoryDto;
import com.hakim.directory.service.mapper.RepositoryMapper;
import com.hakim.directory.utils.GithubApiClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class ContactService {

	private final ContactRepository contactRepository;

	public ContactService(ContactRepository contactRepository) {
		this.contactRepository = contactRepository;
	}

	public boolean addContact(Contact contact) {
		List<RepositoryDto> repoName;
		List<Repository> repositoryList = new ArrayList<>();
		if (contact.getGithub() != null) {
			repoName = GithubApiClient.getRepoName(contact.getGithub());
			repositoryList = RepositoryMapper.repositoryDtosToRepositoryList(repoName);
		}
		try {
			Contact c = contactRepository.saveAndFlush(contact);
			repositoryList.stream().forEach(a -> {
				a.setContact(c);
			});
			contact.setGithubRepoList(repositoryList);
			contactRepository.saveAndFlush(contact);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional(readOnly = true)
	public Contact getContact(Long id)
	{
		return contactRepository.getOne(id);
	}

	@Transactional(readOnly = true)
	public List<Contact> searchContact(ContactDto c) {
		return contactRepository.searchContacts(
				c.getName().isEmpty() ? null : c.getName(),
				c.getEmail().isEmpty() ? null : c.getEmail(),
				c.getGithub().isEmpty() ? null : c.getGithub(),
				c.getOrganization().isEmpty() ? null : c.getOrganization(),
				c.getPhoneNumber().isEmpty() ? null : c.getPhoneNumber());
	}

}
