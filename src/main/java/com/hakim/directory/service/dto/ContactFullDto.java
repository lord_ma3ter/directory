package com.hakim.directory.service.dto;

import java.util.List;

public class ContactFullDto {
	private String name;
	private String phoneNumber;
	private String email;
	private String organization;
	private String github;
	private List<RepositoryDto> gitRepositories;

	public ContactFullDto(String name, String phoneNumber, String email, String organization, String github, List<RepositoryDto> gitRepositories) {
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.organization = organization;
		this.github = github;
		this.gitRepositories = gitRepositories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getGithub() {
		return github;
	}

	public void setGithub(String github) {
		this.github = github;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<RepositoryDto> getGitRepositories() {
		return gitRepositories;
	}

	public void setGitRepositories(List<RepositoryDto> gitRepositories) {
		this.gitRepositories = gitRepositories;
	}

}
