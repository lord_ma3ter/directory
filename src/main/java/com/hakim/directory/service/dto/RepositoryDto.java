package com.hakim.directory.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryDto {

	private String name;

	public RepositoryDto(String name) {
		this.name = name;
	}

	public RepositoryDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
