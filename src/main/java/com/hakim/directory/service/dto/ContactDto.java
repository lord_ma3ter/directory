package com.hakim.directory.service.dto;

public class ContactDto {
	private String name;
	private String phoneNumber;
	private String email;
	private String organization;
	private String github;

	public ContactDto(String name, String email, String organization, String github, String phoneNumber) {
		this.name = name;
		this.email = email;
		this.organization = organization;
		this.github = github;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getGithub() {
		return github;
	}

	public void setGithub(String github) {
		this.github = github;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
