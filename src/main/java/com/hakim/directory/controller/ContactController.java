package com.hakim.directory.controller;

import com.hakim.directory.domain.Contact;
import com.hakim.directory.service.ContactService;
import com.hakim.directory.service.dto.ContactDto;
import com.hakim.directory.service.dto.ContactFullDto;
import com.hakim.directory.service.mapper.ContactMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RestController()
public class ContactController {

	private final ContactService contactService;

	public ContactController(ContactService contactService) {
		this.contactService = contactService;
	}

	@PutMapping(path = "/contacts", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addContact(@RequestBody ContactDto contactDto) {
		if (contactService.addContact(ContactMapper.contactDtoToContact(contactDto))) {
			return new ResponseEntity<>(true, HttpStatus.CREATED);
		}
		return new ResponseEntity<>(false, HttpStatus.NOT_MODIFIED);
	}

	@PostMapping(path = "/contacts/search", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ContactDto>> searchContact(@RequestBody ContactDto contactDto) {
		List<Contact> contacts = contactService.searchContact(contactDto);
		List<ContactDto> contactDtos = ContactMapper.contactsToContactDtos(contacts);
		return new ResponseEntity<>(contactDtos, HttpStatus.OK);
	}

	@GetMapping(path = "/contacts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContactFullDto> getContact(@PathVariable("id") Long id) throws Exception {
		Contact contact = contactService.getContact(id);
		if(contact!=null) {
			ContactFullDto contactFullDto = ContactMapper.contactToContactFullDto(contact);
			return new ResponseEntity<>(contactFullDto, HttpStatus.OK);
		}
		throw  new RuntimeException("there is no such Contact");
	}

	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getContact() {
		return new ResponseEntity<>("welcome!",HttpStatus.OK);
	}

}
