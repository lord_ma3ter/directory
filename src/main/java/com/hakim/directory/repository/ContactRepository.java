package com.hakim.directory.repository;

import com.hakim.directory.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

	@Query("FROM contact c WHERE (:name is null or c.name LIKE %:name%) and (:email is null or c.email LIKE %:email%) " +
			"and (:github is null or c.github LIKE %:github%) and (:organization is null or c.organization LIKE %:organization%)" +
			"and (:phoneNumber is null or c.phoneNumber LIKE %:phoneNumber%)")
	List<Contact> searchContacts(@Param("name") String name,
								 @Param("email") String email,
								 @Param("github") String github,
								 @Param("organization") String organization,
								 @Param("phoneNumber") String phoneNumber
	);

}
