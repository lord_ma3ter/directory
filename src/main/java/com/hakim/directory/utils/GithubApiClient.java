package com.hakim.directory.utils;

import com.hakim.directory.service.dto.RepositoryDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GithubApiClient {

	public static List<RepositoryDto> getRepoName(String githubName) {

		final String resourceUrl = "https://api.github.com/users/" + githubName + "/repos";

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<RepositoryDto>> result =
				restTemplate.exchange(resourceUrl,
						HttpMethod.GET, null, new ParameterizedTypeReference<List<RepositoryDto>>() {
						});

		List<RepositoryDto> gitRepositories = result.getBody();

		String link = result.getHeaders().get("Link").toString();
		link = link.substring(link.indexOf('[') + 1, link.indexOf(']'));
		String[] keyValuePairs = link.split(",");
		Map<String, String> map = new HashMap<>();
		for (String pair : keyValuePairs) {
			String[] entry = pair.split(";");
			map.put(entry[1].trim(), entry[0].trim().substring(link.indexOf('<') + 1, link.indexOf('>')));
		}
		int count = 0;
		while (count != 1) {
			if (map.get("rel=\"next\"").equals(map.get("rel=\"last\""))) {
				count++;
			}
			link = map.get("rel=\"next\"");
			result =
					restTemplate.exchange(link,
							HttpMethod.GET, null, new ParameterizedTypeReference<List<RepositoryDto>>() {
							});

			gitRepositories.addAll(result.getBody());

			link = result.getHeaders().get("Link").toString();
			link = link.substring(link.indexOf('[') + 1, link.indexOf(']'));
			keyValuePairs = link.split(",");

			for (String pair : keyValuePairs) {
				String[] entry = pair.split(";");
				map.put(entry[1].trim(), entry[0].trim().substring(link.indexOf('<') + 1, link.indexOf('>')));
			}
		}

		return gitRepositories;
	}

}
