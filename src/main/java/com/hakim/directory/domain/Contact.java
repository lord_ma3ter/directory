package com.hakim.directory.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "contact")
@Table(name = "contacts")
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_id_seq")
	@SequenceGenerator(name = "contact_id_seq", sequenceName = "contact_id_seq")
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "email")
	private String email;

	@Column(name = "organization")
	private String organization;

	@Column(name = "github")
	private String github;

	@OneToMany(mappedBy = "contact", fetch = FetchType.EAGER,
			cascade = CascadeType.ALL)
	private List<Repository> repositoryList;

	public Contact() {
	}

	public Contact(String name, String email, String phoneNumber, String organization, String github) {
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.organization = organization;
		this.github = github;
	}

	public Contact(String name, String email, String organization, String github) {
		this.name = name;
		this.email = email;
		this.organization = organization;
		this.github = github;
	}

	public Contact(long id, String name, String email, String organization, String github) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.organization = organization;
		this.github = github;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getGithub() {
		return github;
	}

	public void setGithub(String github) {
		this.github = github;
	}

	public List<Repository> getGithubRepoList() {
		return repositoryList;
	}

	public void setGithubRepoList(List<Repository> repositoryList) {
		this.repositoryList = repositoryList;
	}

	@Override
	public String toString() {
		return "Contact{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				", organization='" + organization + '\'' +
				", github='" + github + '\'' +
				'}';
	}
}
