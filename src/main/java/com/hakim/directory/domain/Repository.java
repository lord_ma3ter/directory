package com.hakim.directory.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "repository")
@Table(name = "repositories")
public class Repository implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "repository_id_seq")
	@SequenceGenerator(name = "repository_id_seq", sequenceName = "repository_id_seq")
	private long id;

	@Column(name = "name")
	private String name;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contact_id")
	private Contact contact;

	public Repository() {
	}

	public Repository(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Repository(String name, Contact contact) {
		this.name = name;
		this.contact = contact;
	}

	public Repository(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "GithubRepo{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
