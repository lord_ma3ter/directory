# directory 

Spring boot for testing purpose, Use Java 8 for compile and run this application

**for run application use below command**
```
mvnw install
mvnw spring-boot:run
```

**access to database via below varibles**
```
database web consle url = http://localhost:9090/h2-console
database url = jdbc:h2:mem:directorydb
username = sa
password = password
```


**for add contact from rest api:**

- url

PUT : http://localhost:9090/contacts


- JSON body
```
{
    "name": "ali",
    "phoneNumber":"12345678",
    "email": "ali@msn.com",
    "organization": "my org",
    "github": "burrsutter"
}
```


**for search contact from rest api:**

- url
```
POST : http://localhost:9090/contacts/search


- JSON body
{
    "name": "ali",
    "phoneNumber":"12345678",
    "email": "",
    "organization": "",
    "github": ""
}
```


**for get one contact from rest api:**

- url

```
GET : http://localhost:9090/contacts/{id}
```

id represent contact id



